---
version: 1
titre: Air quality FriLoRaNet
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Serge Ayer
mots-clé: [IoT, Système embarqué, Smart City, LoRa, Air quality]
langue: [F]
confidentialité: non
suite: non
---
## Contexte

Le projet NPR (Nouvelles Politiques Régionales) Fri-LoRanet visant à transformer les villes de Bulle et Fribourg en villes intelligentes a été lancé en décembre de l’année passée. Si le réseau LoRa est déjà opérationnel en ville de Fribourg, il est en développement pour la ville de Bulle.
Le but du réseau LoRa est de contrôler les paramètres importants des deux villes dont l’un est la qualité de l’air. Ce projet de semestre dédié à la qualité de l’air sera subdivisé en deux parties. La première sera le choix de capteurs de température et d’humidité LoRa à la fois précis et bon marché pour un déploiement important. Ceci permettra de tester la capacité du réseau ainsi que de mettre en place et configurer le serveur d’application LoRa.
La deuxième partie sera dédiée à la véritable qualité de l’air ; un capteur de NO2 sera choisi avec les services de l’environnement et sera positionné sur leur site de mesure automatique de Pérolles ou Chamblioux. Le nouveau capteur pourra ainsi être contrôlé et calibré. Il sera ensuite déplacé sur un site intéressant. En fonction du budget à disposition plusieurs capteurs NO2 pourront être déployés.
Les mesures seront analysées en fonction du temps et de la position. Pour les capteurs de température, une cartographie de la chaleur pourra être établie. Ces résultats pourront être communiqués à un autre projet de la ville de Fribourg étudiant des îlots de chaleur au centre-ville. Pour le NO2 chaque point de mesure sera analysé séparément et une comparaison entre les sites pourra ensuite être effectuée.


## Objectifs

1) récupérer par le réseau LoRa Fribourgeois les données des capteurs de la qualité de l'air (NO2)
2) localiser le capteur utilisation d'un gps couplé au capteur et communiquant vers le centre de réception des données
3) installer les 2 capteurs sur le toit d'un véhicule
3) cartographier la qualité de l'air dans la ville de Fribourg grâce aux données émises par le véhicule à travers Fribourg

## Contraintes

équipement: capteur de polution Decentlab est à comprendre et à utiliser, le serveur de réseau LoRa de l'école doit aussi être utilisé
